import urllib, urllib2
import sys, json, os
from urllib import urlencode

def main():
	tmp = [{'bar':'moop'}]
	to_send = urllib.urlencode({'data':tmp})
	myreq = urllib2.Request('http://127.0.0.1:3000/api.json', data=to_send)
	myreq.get_method = lambda: 'POST'
	print urllib2.urlopen(myreq).read()

if __name__ == "__main__":
	main()