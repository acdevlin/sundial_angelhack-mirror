require 'test_helper'

class LogfilesControllerTest < ActionController::TestCase
  setup do
    @logfile = logfiles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:logfiles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create logfile" do
    assert_difference('Logfile.count') do
      post :create, logfile: { name: @logfile.name }
    end

    assert_redirected_to logfile_path(assigns(:logfile))
  end

  test "should show logfile" do
    get :show, id: @logfile
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @logfile
    assert_response :success
  end

  test "should update logfile" do
    put :update, id: @logfile, logfile: { name: @logfile.name }
    assert_redirected_to logfile_path(assigns(:logfile))
  end

  test "should destroy logfile" do
    assert_difference('Logfile.count', -1) do
      delete :destroy, id: @logfile
    end

    assert_redirected_to logfiles_path
  end
end
