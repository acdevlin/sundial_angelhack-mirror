class ApiController < ApplicationController

  respond_to :json
  require 'json'

  def index
  	respond_with(@events = Event.all, :status => :ok)
  end

  def show
    respond_with(:status => :ok)
  end

  def create
  	event_data = JSON.parse(params[:data].gsub("'", '"'))
  	@event = Event.new(event_data.first)
  	@event.save
	render :text => "ok"
  end

end