class LogfilesController < ApplicationController

  # GET /logfiles
  # GET /logfiles.json
  def index
    @logfiles = Logfile.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @logfiles }
    end
  end

  # GET /logfiles/1
  # GET /logfiles/1.json
  def show
    @logfile = Logfile.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @logfile }
    end
  end

  # GET /logfiles/new
  # GET /logfiles/new.json
  def new
    @logfile = Logfile.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @logfile }
    end
  end

  # GET /logfiles/1/edit
  def edit
    @logfile = Logfile.find(params[:id])
  end

  # POST /logfiles
  # POST /logfiles.json
  def create
    @logfile = Logfile.new(params[:logfile])
    file_data = params[:logfile]
    if file_data.respond_to?(:read)
      json_contents = file_data.read
    elsif file_data.respond_to?(:path)
      json_contents = File.read(file_data.path)
    else
      logger.error "Bad file_data: #{file_data.class.name}: #{file_data.inspect}"
    end

    respond_to do |format|
      if @logfile.save
        format.html { redirect_to @logfile, notice: 'Logfile was successfully created.' }
        format.json { render json: @logfile, status: :created, location: @logfile }
      else
        format.html { render action: "new" }
        format.json { render json: @logfile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /logfiles/1
  # PUT /logfiles/1.json
  def update
    @logfile = Logfile.find(params[:id])

    respond_to do |format|
      if @logfile.update_attributes(params[:logfile])
        format.html { redirect_to @logfile, notice: 'Logfile was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @logfile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /logfiles/1
  # DELETE /logfiles/1.json
  def destroy
    @logfile = Logfile.find(params[:id])
    @logfile.destroy

    respond_to do |format|
      format.html { redirect_to logfiles_url }
      format.json { head :no_content }
    end
  end
end
