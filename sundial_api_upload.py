import urllib, urllib2
import sys, json, os
from urllib import urlencode
from datetime import datetime
from time import sleep

LOGPATH = "C:\\Users\\adevlin\\Documents\\Python scripts\\owlwindowlogger-master\\logs"
ONE_MIN = 60
FIVE_SECS = 5

def main(argv):

	#Use provided filename if applicable
	if len(argv) > 1:
		filename = argv[1]
	#Otherwise, get today's log file
	else:
		now = datetime.now()
		filename = now.strftime("%Y-%b-%d.log")

	
	with open(os.path.join(LOGPATH, filename), 'r') as logfile:
		json_data = {}
		json_data['title'] = ""
		json_data['total_time'] = 0
		for line in logfile:
			json_log = json.loads(line)

			if (str(json_data['title']) == str(json_log['ProcName'])):
				#Extend last event instead of creating new one
				json_data['ends_at'] = str(json_log['WinEnd'])
				json_data['total_time'] += int(json_log['TotalIdle'])
				continue;
			else:
				#Send previous event to server if was long enough
				print json_data['total_time']
				if(json_data['total_time'] > ONE_MIN):
					#Rails 'event' model does not have a total_time field
					del json_data['total_time']
					json_complete = [json_data]

					to_send = urllib.urlencode({'data':json_complete})
					myreq = urllib2.Request('http://127.0.0.1:3000/api', data=to_send)
					myreq.get_method = lambda: 'POST'
					print urllib2.urlopen(myreq).read()

					#Give server a quick rest after each upload
					sleep(1)

				#Process new event
				json_data = {}
				json_data['title'] = str(json_log['ProcName'])
				json_data['starts_at'] = str(json_log['WinStart'])
				json_data['ends_at'] = str(json_log['WinEnd'])
				json_data['all_day'] = 'false'
				json_data['description'] = str(json_log['ActiveText'])
				json_data['total_time'] = int(json_log['TotalIdle'])
				json_data['tags'] = []
				json_data['color'] = '#FCFE75'




if __name__ =="__main__":
	main(sys.argv)