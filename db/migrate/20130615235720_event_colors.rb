class EventColors < ActiveRecord::Migration
  def up
  end
  	change_table :events do |t|
  		t.string :color, :default => "blue"
  	end
  def down
  	remove_column :events, :color
  end
end
